## Ornous Labs: http://labs.ornous.com
A website optimised for hosting my experiments and proof of concepts

## Motivation
A platform for me to host exploration, experiments, prototypes and that sort of stuff

## Code style
This project uses prettier and eslint to maintain the standard js code style -

[![js-standard-style](https://img.shields.io/badge/code%20style-standard-brightgreen.svg?style=flat)](https://github.com/feross/standard)
 
## Screenshots
Include logo/demo screenshot etc.

## Tech/framework used
Ex. -

<b>Built with</b>
- [React](https://www.reactjs.org/)
- [Gatsby](https://www.gatsbyjs.org/)
- [Emotion](https://emotion.sh/)
- [Netlify](https://www.netlify.com/docs/)
- [Netlify CMS](https://www.netlifycms.org/)

## License

MIT © [Ornous]()
