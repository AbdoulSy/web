import React from 'react'
import styled from 'react-emotion'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

const Wrapper = styled('footer')`
  grid-area: footer;
  padding: 0 1rem;
  background-color: ${({ theme }) => theme.ornousColor};
  color: white;
  margin: 0;
  font-size: 0.8rem;
`

const Heading = styled('h3')`
  color: #d6d2d2;
  text-align: center;
`

const List = styled('ul')`
  display: grid;
  grid-template-columns: repeat(auto-fit, minmax(2rem, 1fr));
  column-gap: 1rem;
  list-style-type: none;
  max-width: 500px;
  margin: 0 auto;
  padding: 0;
  font-size: 0.85rem;
`

const Link = styled('a')`
  display: flex;
  color: white;
  text-transform: lowercase;
  margin-bottom: 0.26em;
  transition: filter ease 150ms;
  letter-spacing: 0.045em;
  align-items: center;
  justify-content: space-around;
  flex-flow: column;

  &:visited {
    color: white;
  }

  &:hover {
    color: white;
    filter: drop-shadow(0px 0px 0.8px hsla(0, 0%, 100%, 0.8));
  }
`

export const FooterNav = ({ children, heading }) => (
  <React.Fragment>
    {heading && <Heading>{heading}</Heading>}
    <List>{children}</List>
  </React.Fragment>
)

export const NavLink = props => (
  <li>
    <Link {...props} />
  </li>
)

const DimText = styled('div')`
  font-size: 0.7rem;
  color: lighthgray;
  margin: 1em 0;
  text-align: center;
`

export const Copyright = () => (
  <DimText>
    Copyright © 2018 <Link to="/">Ousmane Ndiaye</Link>
  </DimText>
)

export default () => (
  <Wrapper>
    <FooterNav heading="Stay in Touch">
      <NavLink
        target="_blank"
        rel="noopener"
        href="https://www.linkedin.com/in/ornous/"
        title="More about me on LinkedIn"
      >
        <FontAwesomeIcon icon={['fab', 'linkedin']} size="3x" /> LinkedIn
      </NavLink>
      <NavLink
        target="_blank"
        rel="noopener"
        href="https://twitter.com/@ornous"
        title="Follow me on Twitter!"
      >
        <FontAwesomeIcon icon={['fab', 'twitter']} size="3x" /> Twitter
      </NavLink>
      <NavLink
        target="_blank"
        rel="noopener"
        href="https://gitlab.com/ornous/web"
        title="Check out the code for this website!"
      >
        <FontAwesomeIcon icon={['fab', 'gitlab']} size="3x" /> GitLab
      </NavLink>
      <NavLink
        target="_blank"
        rel="noopener"
        href="https://github.com/ornous"
        title="My GitHub Profile"
      >
        <FontAwesomeIcon icon={['fab', 'github']} size="3x" /> GitHub
      </NavLink>
    </FooterNav>
    <Copyright />
  </Wrapper>
)
