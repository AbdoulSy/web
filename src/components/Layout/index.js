import React from 'react'
import PropTypes from 'prop-types'
import Helmet from 'react-helmet'
import { StaticQuery, graphql } from 'gatsby'
import { ThemeProvider } from 'emotion-theming'
import styled from 'react-emotion'

import { Grid, GridContainer, MainArea, Header } from '../Grid'
import theme from '../../themes'

export const Content = styled('div')`
  margin: 0 auto;
  max-width: ${({ fullWidth }) => (fullWidth ? '90vw' : '960px')};
`

const Layout = ({ children, hero }) => (
  <StaticQuery
    query={graphql`
      query SiteTitleQuery {
        site {
          siteMetadata {
            title
          }
        }
      }
    `}
    render={data => (
      <>
        <Helmet
          title={data.site.siteMetadata.title}
          link={[
            {
              rel: 'icon',
              href: data.site.siteMetadata.favicon,
              type: 'image/x-icon',
            },
          ]}
          meta={[
            { name: 'description', content: 'Ornous' },
            { name: 'keywords', content: 'Ornous, Ousmane, Ndiaye, developer' },
          ]}
        >
          <html lang="en" />
        </Helmet>
        <ThemeProvider theme={theme}>
          <GridLayout Hero={hero}>{children}</GridLayout>
        </ThemeProvider>
      </>
    )}
  />
)

Layout.propTypes = {
  children: PropTypes.node.isRequired,
  hero: PropTypes.element,
}

export const GridLayout = ({ children, Hero }) => {
  return (
    <GridContainer>
      <Grid>
        <Header>
          {Hero && Hero}
        </Header>
        <MainArea>{children}</MainArea>
      </Grid>
    </GridContainer>
  )
}

export default Layout
