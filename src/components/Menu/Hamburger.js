import React from 'react'
import styled from 'react-emotion'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

const Hamburger = styled('div')`
  grid-area: hamburger;
  font-family: lato;
  color: ${({ theme }) => theme.ornousColor};
  font-size: 2em;

  @media (min-width: 700px) {
    display: none;
  }
`

export default () => (
  <Hamburger>
    <FontAwesomeIcon icon="bars" />
  </Hamburger>
)
