import styled from 'react-emotion'

export const Heading = styled('h3')`
  grid-area: heading;
  text-transform: uppercase;
  letter-spacing: 0.1rem;
  color: ${({ theme }) => theme.ornousColor};
  font-size: 1.1rem;
  margin: 0;
  height: inherit;

  a {
    text-decoration: none;
    color: inherit;
  }
`
