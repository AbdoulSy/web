import styled from 'react-emotion'

export const List = styled('ul')`
  grid-area: menu;
  list-style-type: none;
  text-indent: 0;
  text-align: center;
  margin: 0;
  padding: 0;
  font-family: lato;
  display: none;
  justify-content: center;
  align-items: center;

  @media (min-width: 700px) {
    display: flex;
  }
`
