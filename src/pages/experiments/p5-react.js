import React from 'react'
import PropTypes from 'prop-types'
import { graphql } from 'gatsby'
import Helmet from 'react-helmet'
import styled from 'react-emotion'
import Layout from '../../components/Layout'

const isBrowser = typeof window !== 'undefined'
const p5 = isBrowser ? require('p5') : undefined
if (isBrowser) require('p5/lib/addons/p5.sound')


const Content = styled('div')`
  margin: 0 auto;
  max-width: 960px;
  padding: 0 3vw;
`

const intervalsToNotes = (intervals, key = 40) => {
  return intervals.reduce((notes, interval) => {
    if (interval === 0) {
      notes.push(key)
      return notes
    }

    const prevNote = [notes.slice(-1)]
    notes.push(parseInt(prevNote, 10) + interval)
    return notes
  }, [])
}

const pitches = {
  C: 0,
  D: 2,
  E: 4,
  F: 5,
  G: 7,
  A: 9,
  B: 11,
}

const fQNote = (pitch) => {
  const [note, octave] = pitch.split('')
  return ((octave - 2) * 12) + pitches[note]
}

class Sketch extends React.PureComponent {
  constructor(props) {
    super(props)
    this.canvasWrapper = React.createRef()
  }

  componentDidMount() {
    new p5((p) => {
      const majorIntervals = [0, 2, 2, 1, 2, 2, 2, 1] // eslint-disable-line
      const naturalMinorIntervals = [0, 2, 1, 2, 2, 1, 2, 2] // eslint-disable-line
      const harmonicMinorIntervals = [0, 2, 1, 2, 2, 1, 3, 1] // eslint-disable-line
      const melodicMinorIntervals = [0, 2, 1, 2, 2, 2, 2, 1] // eslint-disable-line

      const harmonize = true
      const randomize = true
      const modes = {
        ionian: [0, 2, 2, 1, 2, 2, 2, 1], // Bright, happy: C-F-G
        dorian: [0, 2, 1, 2, 2, 2, 1, 2], // Dark, yet sweet: Dm6-F
        phrygian: [0, 1, 2, 2, 2, 1, 2, 2], // Dark, exotic: Em-F
        lydian: [0, 2, 2, 2, 1, 2, 2, 1], // Bright, mysterious: Fmaj7#11-C
        mixolydian: [0, 2, 2, 1, 2, 2, 1, 2], // Bright with a dark edge: G-F
        aeolian: [0, 2, 1, 2, 2, 1, 2, 2], // Dark, sad: Am-G-F
        locrian: [0, 1, 2, 1, 1, 2, 2, 2], // Discordant, unresolved: Bm7b5-Em
      }
      const key = fQNote('G3')

      // const scaleArray = intervalsToNotes(majorIntervals)
      // const scaleArray = intervalsToNotes(naturalMinorIntervals)
      // const scaleArray = intervalsToNotes(harmonicMinorIntervals)
      // const scaleArray = intervalsToNotes(melodicMinorIntervals)
      // const scaleArray = intervalsToNotes(modes.ionian)
      // modes: ionian, dorian, phrygian, lydian, mixolydian, aeolian, locrian
      const scaleArray = intervalsToNotes(modes.mixolydian)
      // const scaleArray = ['D4', 'C4', 'G4', 'G4', 'D4', 'C4', 'C4', 'G4'].map(fQNote)
      //
      const RootOsc = new p5.SawOsc()
      const ThirdOsc = new p5.SawOsc()
      const SubOsc = new p5.SawOsc()

      const RootEnv = new p5.Envelope()
      const SubEnv = new p5.Envelope()

      let note = 0
      const fft = new p5.FFT()

      const eq = new p5.EQ()
      const filter = new p5.BandPass()
      let beat

      p.setup = () => {
        filter.res(50)
        eq.bands[0].gain(-0) // Low
        eq.bands[1].gain(-0)
        eq.bands[2].gain(-0)
        eq.chain(filter)
        const attack = 0.611
        const decay = 0.2
        const sustain = 0.05
        const release = 0.411
        RootEnv.setADSR(attack, decay, sustain, release);
        SubEnv.setADSR(attack, decay, sustain, release);

        // set attackLevel, releaseLevel
        RootEnv.setRange(1, 0.1);
        SubEnv.setRange(.3, 0);

        p.createCanvas(1000, 600)
        RootOsc.amp(1)
        RootOsc.mult(0)
        RootOsc.pan(-1)
        RootOsc.freq(0)

        ThirdOsc.amp(1)
        ThirdOsc.mult(0)
        ThirdOsc.pan(1)
        ThirdOsc.freq(0)


        SubOsc.freq(0)
        SubOsc.amp(0)

        RootOsc.disconnect()
        RootOsc.connect(filter)
        RootOsc.start()

        ThirdOsc.disconnect()
        ThirdOsc.connect(filter)
        ThirdOsc.start()

        SubOsc.disconnect()
        SubOsc.connect(filter)
        SubOsc.start()

        p.masterVolume(23)
        p.frameRate(80) // BPM
        p.noStroke();
        beat = 80
      }

      p.draw = () => {
        p.background(20)
        p.fill(120)

         if (p.frameCount % beat === 0 || p.frameCount === 1) {
           const midiValue = scaleArray[note] + key
           const freqValue = p.midiToFreq(midiValue)
           const release = randomize ? (p.random(0, 1) > .5 ? 0 : 0.5) : 0
           RootOsc.freq(freqValue)
           RootEnv.play(RootOsc, 0)
           ThirdOsc.freq(freqValue)
           RootEnv.play(ThirdOsc, 0)
           filter.freq(freqValue)

           ThirdOsc.freq(freqValue + 1)
           RootEnv.play(ThirdOsc, 0, release)

           SubOsc.freq(p.midiToFreq(midiValue - 24))
           SubEnv.play(SubOsc, 0, release / 2)


           if (harmonize) {
             let chord = {
               root: note,
               third: (note + 3) % scaleArray.length,
               fifth: (note + 5) % scaleArray.length,
               seventh: (note + 7) % scaleArray.length
             }
             RootOsc.freq(p.midiToFreq(scaleArray[chord.third] + key))
             RootEnv.play(RootOsc, 0, release)
             RootOsc.freq(p.midiToFreq(scaleArray[chord.fifth] + key))
             RootEnv.play(RootOsc, 0, release)
             ThirdOsc.freq(p.midiToFreq(scaleArray[chord.third] + key))
             RootEnv.play(ThirdOsc, 0, release)
             ThirdOsc.freq(p.midiToFreq(scaleArray[chord.fifth] + key))
             RootEnv.play(ThirdOsc, 0, release)
           }

           note = randomize
             ?  Math.floor(p.random(0, scaleArray.length - 1))
             : note = (note + 1) % scaleArray.length;
        }

        const spectrum = fft.analyze();
        for (let i = 0; i < spectrum.length / 20; i++) {
          p.fill(spectrum[i], spectrum[i] / 10, 0);

          const x = p.map(i, 0, spectrum.length / 20, 0, p.width);
          const h = p.map(spectrum[i], 0, 255, 0, p.height);
          p.rect(x, p.height, spectrum.length/20, -h);
        }
      }
    }, this.canvasWrapper.current)
  }

  render() {
    return <div ref={this.canvasWrapper}></div>
  }
}

const P5ReactPage = ({
  data: {
    site: { siteMetadata: { title } },
    heroImage: {
      childImageSharp: { fluid: heroImageData },
    },
  },
}) => {
  return (
    <Layout>
      <Helmet title={`P5 w/ React | ${title}`} />
      <Content>
        <p>P5 w/ React</p>
        <Sketch />
      </Content>
    </Layout>
  )
}

P5ReactPage.propTypes = {
  data: PropTypes.shape({
    site: PropTypes.shape({
      siteMetadata: PropTypes.shape({
        title: PropTypes.string.isRequired,
      }),
    }),
    heroImage: PropTypes.shape({
      childImageSharp: PropTypes.shape({}),
    }),
  }),
}

export default P5ReactPage

export const pageQuery = graphql`
  query P5ReactQuery {
    site {
      siteMetadata {
        title
      }
    }
    heroImage: file(relativePath: { eq: "projects/gtd.jpg" }) {
      childImageSharp {
        fluid(maxWidth: 1400, maxHeight: 850) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`
