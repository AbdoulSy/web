import 'typeface-lato'
import 'typeface-montserrat'

export default {
  name: 'Warm Antique',
  colors: {
    panelBgColor: '#E2B091',
    contentBgColor: 'white',
    headingFgColor: '#873E4C',
    ornousColor: '#873E4C',
    oColor: '#873E4C',
    zColor: '#F7DFD4',
    menuBgColor: '#EABCAC',
    textColor: '#4E2025',
    menuFgColor: '#873E4C',
  },
  typefaces: {
    heading: 'Montserrat',
    legibleText: 'Lato',
  },
}
